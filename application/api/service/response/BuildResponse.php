<?php

/**
 *  +----------------------------------------------------------------------
 *  | 草帽支付系统 [ WE CAN DO IT JUST THINK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2018 http://www.iredcap.cn All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( https://www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: Brian Waring <BrianWaring98@gmail.com>
 *  +----------------------------------------------------------------------
 */

namespace app\api\service\response;
use app\common\library\HttpHeader;
use mysql_xdevapi\Result;
use think\exception\HttpResponseException;
use think\Log;
use think\Response;
use app\common\library\CodeGenerate;

class BuildResponse extends ApiSend
{


    /**
     *
     * @author 勇敢的小笨羊 <brianwaring98@gmail.com>
     *
     * @param array $chargeRespose
     * @throws \app\common\library\exception\ParameterException
     */
    public function doBuild($chargeRespose)
    {
        http_response_code(200);    //设置返回头部
        //增加收银台id并生成url，兼容各支付渠道
        $pay_id=CodeGenerate::getCode(32,'');

        $return['result_code'] = 'OK';
        $return['result_msg'] = empty($chargeRespose) ? 'FAIL' : 'SUCCESS';
        $return['pay_url'] = url('/cashier/pay',['pay_id'=> $pay_id],'',true);
        $return['charge'] =  self::get('ApiResposeData');



        Log::notice('Response Data :' . json_encode($return));

        //签名及数据返回
        $response = Response::create(json_encode($return))->header(self::get('header'));
        // 销毁请求上下文
        self::destoryContext();

        //记录回应数据便于收银台直接调用
        $return['pay_id'] = $pay_id;
        $this->logicOrdersResult->createOrderResult($return);
        // 数据响应
        throw new HttpResponseException($response);
    }


}