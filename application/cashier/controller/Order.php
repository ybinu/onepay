<?php
/**
 *  +----------------------------------------------------------------------
 *  | 草帽支付系统 [ WE CAN DO IT JUST THINK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2018 http://www.iredcap.cn All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( https://www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: Brian Waring <BrianWaring98@gmail.com>
 *  +----------------------------------------------------------------------
 */

namespace app\cashier\controller;

use app\common\controller\Common;
use app\common\library\Activation;
use think\Log;
use think\Request;

class Order extends BaseCashier
{
    /**
     * 访问首页  -  加载框架
     *
     *
     * @return mixed
     */
    public function pay()
    {
        $param= input();

        if (!$this->validateOrder->check($param)) {
            $this->error($this->validateOrder->getError());
        }

        $order=db('orders')
            ->alias('o')
            ->join('orders_result r','o.out_trade_no=r.out_trade_no','left')
            ->where(['pay_id'=>$param['pay_id']])
            ->field(['o.*','r.id as rid','r.pay_id','r.result'])
            ->find();

        if($order['status']=='2')  $this->success('您的订单支付成功，正在跳转...',$order['return_url']);

        $result=json_decode($order['result'],true);

        $this->assign('order',$order);
        $this->assign('result',$result);
        $this->assign('apijson',json_encode($result['credential']));

        if($order['channel']=='wx_native'||$order['channel']=='ali_qr'){
            return $this->fetch('order/native');
        }elseif($order['channel']=='wx_jsapi'){
            return $this->fetch('order/index');
        }elseif ($order['channel']=='wx_h5'){
            return $this->fetch('order/h5');
        }
        else{
            $this->error('没有此订单','/index/');
        }


    }



}
