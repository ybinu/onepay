<?php


namespace app\cashier\validate;


class Order extends Base
{
    /**
     * API数据规则
     *
     * @author 勇敢的小笨羊 <brianwaring98@gmail.com>
     *
     * @var array
     */
    protected $rule = [
        'pay_id'         => 'require|isNotEmpty',
    ];

    protected $message  =   [
        'pay_id.require'    => '付款码不能为空',
        'pay_id.number'      => '付款码',
        'pay_id.length'     => '付款码最少15位'
    ];
}