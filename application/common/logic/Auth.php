<?php
/**
 *  +----------------------------------------------------------------------
 *  | 草帽支付系统 [ WE CAN DO IT JUST THINK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2018 http://www.iredcap.cn All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( https://www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: Brian Waring <BrianWaring98@gmail.com>
 *  +----------------------------------------------------------------------
 */

namespace app\common\logic;

use app\common\library\enum\CodeEnum;
use think\Db;
use think\Log;
use think\Session;
class Auth extends BaseLogic
{

    /**
     * 下单时通过pay_code 获取渠道下的可用商户配置
     *
     * @author 勇敢的小笨羊 <brianwaring98@gmail.com>
     *
     * @param $order
     * @return mixed
     */
    public function getAllowedAccount($channel='wx_auth'){
        //1.传入支付方式获取对应渠道cnl_id
        $codeInfo = $this->modelPayCode->getInfo(['code' => $channel], 'id as co_id,cnl_id')->toArray();

        //2.cnl_id获取支持该方式的渠道列表
        $channels = $this->modelPayChannel->getColumn(['id' => ['in', $codeInfo['cnl_id']], 'status' => ['eq','1']],
            'id,name,action,timeslot,return_url,notify_url');

        //3.规则排序选择合适渠道
        /*******************************/
        //TODO 写选择规则  时间、状态、费率 等等
        //规则处理  我先简便写一下
        $channelsMap = [];
        foreach ($channels as $key => $val){
            $timeslot = json_decode($val['timeslot'],true);
            if ( strtotime($timeslot['start']) < time() && time() < strtotime($timeslot['end']) ){
                $channelsMap[$key] = $val;
            }
        }

        //判断可用
        if (empty($channelsMap)){
            return ['errorCode' => '400006','msg' => 'Route Payment Error. [No available channels]'];
        }
        $channel =  $channelsMap[array_rand($channelsMap)];

        /*******************************/
        //3.获取该渠道下可用账户
        $accounts = $this->modelPayAccount->getColumn(['cnl_id' => ['eq',$channel['id']], 'status' => ['eq','1']],
            'id,co_id,name,single,daily,timeslot,param');

        //4.规则取出可用账户
        /*******************************/
        //TODO 写选择规则  时间、状态、费率 等等
        //规则处理  我先简便写一下
        $accountsMap = [];
        foreach ($accounts as $key => $val){
            $timeslot = json_decode($val['timeslot'],true);
            if ( in_array($codeInfo['co_id'], str2arr($val['co_id'])) && strtotime($timeslot['start']) < time() && time() < strtotime($timeslot['end']) ){
                $accountsMap[$key] = $val;
            }
        }

        //判断可用
        if (empty($accountsMap)){
            return ['errorCode' => '400008','msg' => 'Route Payment Error. [No available merchants account.]'];
        }

        $account =  $accountsMap[array_rand($accountsMap)];
        $accountConf = json_decode($account['param'],true);
        //判断配置是否正确
        if (is_null($accountConf)){
            return ['errorCode' => '400008','msg' => 'Route Payment Error. [Payment account was misconfigured.]'];
        }
        //配置合并
        $configMap = array_merge($channel, $accountConf);

        Session::set('auth_wx_config',$configMap);

        return $configMap;

    }


}